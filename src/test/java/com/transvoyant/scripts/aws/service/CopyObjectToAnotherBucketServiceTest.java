package com.transvoyant.scripts.aws.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.VersionListing;
import com.google.common.collect.Lists;
import com.transvoyant.scripts.aws.model.InputParameters;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CopyObjectToAnotherBucketServiceTest {

    private static final String SOURCE_BUCKET = "stag";
    private static final String SOURCE_PREFIX = "po/";
    private static final String DESTINATION_BUCKET = "prod";
    private static final String DESTINATION_PREFIX = "sbd/";
    private static final String FILE1 = "file1";
    private static final String FILE2 = "file2";
    private static final String V1 = "v1";
    private static final String V2 = "v2";
    private static final String V3 = "v3";

    @Mock
    private AmazonS3 s3Client;
    @Mock
    private VersionListing versionListing;
    @Mock
    private S3VersionSummary summary1;
    @Mock
    private S3VersionSummary summary2;
    @Mock
    private S3VersionSummary summary3;
    @Mock
    private S3VersionSummary summary4;
    @Mock
    private S3VersionSummary summary5;
    @Mock
    private S3Object s3Object1;
    @Mock
    private S3Object s3Object2;

    @Before
    public void before() {
        GetObjectRequest getObjectRequest1 = new GetObjectRequest(SOURCE_BUCKET, SOURCE_PREFIX + FILE1, "v2");
        GetObjectRequest getObjectRequest2 = new GetObjectRequest(SOURCE_BUCKET, SOURCE_PREFIX + FILE2, "v3");

        when(s3Object1.getKey()).thenReturn(SOURCE_PREFIX + FILE1);
        when(s3Object2.getKey()).thenReturn(SOURCE_PREFIX + FILE2);

        mockSummary(summary1, V1, true, true, SOURCE_PREFIX + FILE1);
        mockSummary(summary2, V2, true, false, SOURCE_PREFIX + FILE1);
        mockSummary(summary3, V1, true, true, SOURCE_PREFIX + FILE2);
        mockSummary(summary4, V3, true, false, SOURCE_PREFIX + FILE2);
        mockSummary(summary5, V1, false, false, "");

        when(versionListing.isTruncated()).thenReturn(true);
        when(versionListing.getVersionSummaries()).thenReturn(
                Lists.newArrayList(summary1, summary2, summary3, summary4, summary5));

        when(s3Client.listVersions(any(ListVersionsRequest.class))).thenReturn(versionListing);
        when(s3Client.listNextBatchOfVersions(any(VersionListing.class))).thenReturn(new VersionListing());

        when(s3Client.getObject(eq(getObjectRequest1))).thenReturn(s3Object1);
        when(s3Client.getObject(eq(getObjectRequest2))).thenReturn(s3Object2);
    }

    @Test
    public void copyLatestDeletedFilesTest() {
        CopyObjectToAnotherBucketService service = new CopyObjectToAnotherBucketService(s3Client);

        service.copyFiles(new InputParameters()
                .setSourceBucket(SOURCE_BUCKET)
                .setSourcePrefix(SOURCE_PREFIX)
                .setDestinationBucket(DESTINATION_BUCKET)
                .setDestinationPrefix(DESTINATION_PREFIX)
                .setStartDate("2018-08-01T00:00:00Z"));

        ArgumentCaptor<CopyObjectRequest> requestCaptor = ArgumentCaptor.forClass(CopyObjectRequest.class);
        verify(s3Client, times(2)).copyObject(requestCaptor.capture());

        List<CopyObjectRequest> requests = requestCaptor.getAllValues();

        assertEquals(2, requests.size());
        verifyRequest(requests.get(0), SOURCE_PREFIX + FILE1, "v2", DESTINATION_PREFIX + SOURCE_PREFIX + FILE1);
        verifyRequest(requests.get(1), SOURCE_PREFIX + FILE2, "v3", DESTINATION_PREFIX + SOURCE_PREFIX + FILE2);
    }

    private void mockSummary(S3VersionSummary summaryMock, String versionId, boolean isDeleteMarker, boolean isLatest,
            String key) {
        Date date = DateTime.now().toDate();

        when(summaryMock.getVersionId()).thenReturn(versionId);
        when(summaryMock.isDeleteMarker()).thenReturn(isDeleteMarker);
        when(summaryMock.isLatest()).thenReturn(isLatest);
        when(summaryMock.getKey()).thenReturn(key);
        when(summaryMock.getLastModified()).thenReturn(date);
        when(summaryMock.getSize()).thenReturn(10L);
    }

    private void verifyRequest(CopyObjectRequest request, String sourceKey, String sourceVersionId,
            String destinationKey) {
        assertEquals(request.getSourceBucketName(), SOURCE_BUCKET);
        assertEquals(request.getDestinationBucketName(), DESTINATION_BUCKET);
        assertEquals(request.getSourceKey(), sourceKey);
        assertEquals(request.getSourceVersionId(), sourceVersionId);
        assertEquals(request.getDestinationKey(), destinationKey);
    }
}