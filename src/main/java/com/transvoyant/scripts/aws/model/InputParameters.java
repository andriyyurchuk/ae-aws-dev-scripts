package com.transvoyant.scripts.aws.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class InputParameters {

    private String sourceBucket;
    private String sourcePrefix;
    private String destinationBucket;
    private String destinationPrefix;

    private String startDate;

}
