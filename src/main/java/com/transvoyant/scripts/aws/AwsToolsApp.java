package com.transvoyant.scripts.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.transvoyant.scripts.aws.model.InputParameters;
import com.transvoyant.scripts.aws.service.CopyObjectToAnotherBucketService;
import org.apache.commons.lang3.StringUtils;

public class AwsToolsApp {

    public static void main(String[] args) {
        InputParameters params = getInputParameters(args);

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                                                 .withCredentials(new AWSStaticCredentialsProvider(getAwsCreds()))
                                                 .withRegion(Regions.US_EAST_1)
                                                 .build();

        new CopyObjectToAnotherBucketService(s3Client).copyFiles(params);
    }

    private static BasicAWSCredentials getAwsCreds() {
        if (StringUtils.isEmpty(System.getenv("AWS_KEY")) || StringUtils.isEmpty(System.getenv("AWS_SECRET"))) {
            throw new IllegalArgumentException("Unable to find AWS_KEY or AWS_SECRET");
        }
        return new BasicAWSCredentials(System.getenv("AWS_KEY"), System.getenv("AWS_SECRET"));
    }

    private static InputParameters getInputParameters(String[] args) {
        if (args == null || args.length != 4) {
            throw new IllegalArgumentException("Not enough arguments");
        } else {
            return new InputParameters()
                    .setSourcePrefix(args[0])
                    .setSourceBucket(args[1])
                    .setDestinationPrefix(args[2])
                    .setDestinationBucket(args[3])
                    .setStartDate("2018-07-09T00:00:00Z");
        }
    }
}
