package com.transvoyant.scripts.aws.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.VersionListing;
import com.transvoyant.scripts.aws.model.InputParameters;

import java.time.Instant;

public class CopyObjectToAnotherBucketService {

    private final AmazonS3 s3Client;

    public CopyObjectToAnotherBucketService(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public void copyFiles(InputParameters params) {
        long startDate = Instant.parse(params.getStartDate()).toEpochMilli();

        ListVersionsRequest listObjectsRequest = new ListVersionsRequest()
                .withBucketName(params.getSourceBucket())
                .withPrefix(params.getSourcePrefix())
                .withMaxResults(1000);

        String lastKey = null;
        boolean lastDeleted = false;

        VersionListing versionListing = s3Client.listVersions(listObjectsRequest);
        while (true) {
            for (S3VersionSummary objectSummary : versionListing.getVersionSummaries()) {
                System.out.printf("Retrieved object %s, version %s, isLatest %s, isDeleteMarker %s, size %s, Date %s\n",
                        objectSummary.getKey(),
                        objectSummary.getVersionId(),
                        objectSummary.isLatest(),
                        objectSummary.isDeleteMarker(),
                        objectSummary.getSize(),
                        objectSummary.getLastModified() == null ? "" :
                                objectSummary.getLastModified().toInstant().toString());

                if (objectSummary.getKey() != null && objectSummary.getKey().equals(lastKey)) {
                    if (objectSummary.getLastModified() != null
                            && startDate < objectSummary.getLastModified().getTime()
                            && objectSummary.getSize() > 0
                            && lastDeleted) {
                        S3Object object = s3Client.getObject(
                                new GetObjectRequest(params.getSourceBucket(), objectSummary.getKey(),
                                        objectSummary.getVersionId()));
                        if (object != null) {
                            // copy object to a new bucket
                            CopyObjectRequest copyObjectRequest =
                                    new CopyObjectRequest(params.getSourceBucket(), object.getKey(),
                                            objectSummary.getVersionId(), params.getDestinationBucket(),
                                            params.getDestinationPrefix() + object.getKey());
                            try {
                                s3Client.copyObject(copyObjectRequest);
                            } catch (RuntimeException e) {
                                System.out.println("## Failed to copy object");
                            }
                        } else {
                            System.out.println(String.format("Document %s not found", objectSummary.getKey()));
                        }
                    }
                } else if (objectSummary.isLatest()) {
                    lastKey = objectSummary.getKey();
                    lastDeleted = objectSummary.isDeleteMarker();
                }
            }
            if (versionListing.isTruncated()) {
                versionListing = s3Client.listNextBatchOfVersions(versionListing);
            } else {
                break;
            }
        }
    }

}
