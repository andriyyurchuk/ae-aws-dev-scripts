# ae-aws-dev-scripts

Required environment variables:
- `AWS_KEY`
- `AWS_SECRET`

Incoming arguments:
- `Source prefix` (*tv-test-sbd*)
- `Source bucket` (*purchaseorder*)
- `Destination prefix` (*sbd*)
- `Destination bucket` (*tv-prod-p2l-integration-sftp-inbound*)
- `Start date` (*2018-07-09T00:00:00Z*)

